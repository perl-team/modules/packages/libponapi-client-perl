Source: libponapi-client-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Andrius Merkys <merkys@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: libhijk-perl <!nocheck>,
                     libjson-maybexs-perl <!nocheck>,
                     libmoose-perl <!nocheck>,
                     liburi-perl <!nocheck>,
                     libyahc-perl <!nocheck>,
                     perl
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libponapi-client-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libponapi-client-perl.git
Homepage: https://metacpan.org/release/PONAPI-Client

Package: libponapi-client-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libhijk-perl,
         libjson-maybexs-perl,
         libmoose-perl,
         liburi-perl,
         libyahc-perl
Description: client to a JSON:API v1.0 service
 PONAPI::Client is a JSON:API compliant client; it should be able to communicate
 with any API-compliant service.
 .
 The client does a handful of checks required by the spec, then uses Hijk to
 communicate with the service.
 .
 In most cases, all API methods return a response document:
 .
 my $response = $client->retrieve(...);
 .
 In list context however, all API methods will return the request status and
 the document:
 .
 my ($status, $response) = $client->retrieve(...)
